import React,{Component, Fragment} from 'react'
import axios from 'axios'
import AvatarPict from '../atom/Avatar'
const style={
    box:{
        padding: 15,
        backgroundColor:'#aedefc',
        margin: 10
    },
    div1:{
        width:'15%', 
        padding:5, 
        display:'inline-block',
        verticalAlign: 'middle'
    },
    div2:{
        width:'75%', 
        padding:5, 
        display:'inline-block',
        verticalAlign: 'middle'
    }
}
class Show extends Component{
    constructor(props){
        super(props)
        this.state = {
            members:[]
        }
    }
    componentDidMount() {
        const { match } = this.props
        axios.get(`https://swapi.co/api/people/${match.params.id}`)
            .then( ({ data }) => {
                this.setState({
                    members: data
                })
        })
    }
    render(){
        const {members} = this.state
        return(
            <div style={{margin:100}}>
                <Fragment>
                    <div style={style.box}>
                        <div style={style.div1}>
                            <AvatarPict />
                        </div>
                        <div style={style.div2}>
                            <b>{members.name}</b>
                            <br/>
                            <p>{members.height} cm </p>
                            <br/>
                            <p>{members.mass}</p>
                        </div>
                    </div>
                </Fragment>
            </div>
        )
    }
}
export default Show