import React from 'react'
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

const style={
    box:{
        margin:100,
        padding: 15,
        backgroundColor:'#aedefc'
    }
}
const AboutEvent = () =>{
    return(
        <Paper style={style.box} elevation={1}>
            <Typography variant="h6" color="inherit">
                <b>About QTemu</b>
            </Typography>
            <hr/>
            <Typography component="p">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, 
                sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris 
                nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
                reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
                Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia 
                deserunt mollit anim id est laborum.
            </Typography>
        </Paper>
    )
}
export default AboutEvent