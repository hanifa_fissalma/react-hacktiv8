import React, { Fragment } from 'react'
import Profile from '../molecule/Profile'
import Members from '../molecule/Members'
import NextMeetup from '../molecule/NextMeetup'
import About from '../molecule/About'
import PastMeetup from '../organism/PastMeetup'
import SubMenu from '../molecule/SubMenu'
import axios from 'axios'
import {BrowserRouter as Route, Link} from 'react-router-dom'
class Home extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      pastmeetup : [],
      members : [], 
      profiles : [
        {
          id:1,
          location:'Jakarta, Indonesia',
          member: 2000,
          organizer:'Hani',
          image:'https://lumiere-a.akamaihd.net/v1/images/5c239685ecbea2c12fcb8ee661a04d0a9098e141.jpeg'
        }
      ], nextmeetup : [
        {
          id: 1,
          title: 'Next Meetup',
          subtitle: 'Awesome Meetup and Event', 
          date : '25 January 2019', 
          members:'Hengki, Giovani, Sofian, Riza, Agung', 
          organization : 'The JakartaJS Organizers', 
          message : 'Hai World'
        }
      ], about :[
        {
          message:'Come and meet other developers interested in the Javascript and its library in the Greater Jakarta area',
          socialMedia: 'Twitter',
          account : '@jakartajs',
          hashtag: '#jakartajs'
        }
      ]
    }
  }
  componentDidMount(){
    //fetch data members
    axios.get('https://swapi.co/api/people/')
      .then(response => response.data.results)
      .then(members => {
        this.setState({
          members:members
        })
      })
    //fetch data pastmeetup
    axios.get('https://swapi.co/api/films/')
      .then(response => response.data.results)
      .then(pastmeetup => {
        this.setState({
          pastmeetup:pastmeetup
        })
      })


    /*
    alternatif 

    const arr=[
      () => axios.get()
      () => axios.get()
    ]

    Promise.all(arr)
      .then(response => {
        response[0]
        response[1]
      })
    */
  }
  render() {
    const {profiles, nextmeetup, about} = this.state
    return (
      <Fragment>
        <div className="section">
          {profiles
            .map((data, index) => (
            <Profile key={index} {...data} />
            ))
          }
          <br/>
          {nextmeetup
            .map((data, index) => (
            <NextMeetup key={index} {...data} />
            ))
          }
          <br/>
          {about
            .map((data, index) => (
            <About key={index} {...data} />
            ))
          }
          <br/>
          <SubMenu title="Members" />
          {this.state.members
            .slice(0,3)
            .map((member,index)=> (
                <Route>
                  <Link to={`show/${index+1}`}><Members key={member.name} name={member.name} height={member.height}/></Link>
                </Route>
              ) 
            )
          }
          
          <br/>
          <SubMenu title="Past Meetups" />
          {this.state.pastmeetup
            .slice(0,6)
            .map((pastmeetup, index) => (
            <PastMeetup 
              key={index} 
              title={pastmeetup.title} 
              countMeet={pastmeetup.episode_id} 
              titleMeet={pastmeetup.director} 
              countPeople={pastmeetup.episode_id} />
            ))
          }
          <br/><br/>
        </div>
      </Fragment>
    );
  }
}

export default Home;
