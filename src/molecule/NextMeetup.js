import React , {Component, Fragment} from 'react'
import Title from '../atom/Title'
const style={
    box:{
        padding: 15,
        backgroundColor:'#aedefc'
    },
    date:{
        color: '#fb929e',
        fontSize:'0.9em',
        padding: 0,
        fontWeight: 'bold',
        marginBottom:5
    }
}
class NextMeetup extends Component{
    render(){
        const {title, subtitle, date, members, organization, message} = this.props
        return(
            <Fragment>
                <Title variant="h5"><b>{title}</b></Title>
                <div style={style.box}>
                    <Title variant="h6">{subtitle}</Title>
                    <div style={style.date}>{date}</div>
                    <p>{message}</p>
                    <p>Best, {members}, {organization}
                    </p>
                </div>
            </Fragment>
        )
    }
}
export default NextMeetup