import React from 'react'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button2 from '../atom/Button'
import { Link } from "react-router-dom"
import {connect} from 'react-redux'

const styles= {
    login:{
        color:'white',
        float:'right',
        boxShadow:'none',
    }
}
let Header = (props) => {
    return(
        <div style={{flexGrow:1}}>
            <AppBar style={{backgroundColor:'#f73378'}}>
                <Toolbar>
                    <Typography variant="h5" color="inherit" style={{flexGrow:1}}><b>{props.app.pageTitle}</b></Typography>
                    <Link to="/about" ><Button2 style={styles.login}>About</Button2></Link>
                    <Link to="/" ><Button2 style={styles.login}>Home</Button2></Link>

                    {/* <Button2 style={styles.login}>Login</Button2> */}
                </Toolbar>
            </AppBar>
        </div>
    )
}
const mapStateToProps = (state) =>{
    return{
        app: state.app
    }
}
export default connect(mapStateToProps)(Header)