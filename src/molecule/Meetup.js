import React, {Component, Fragment} from 'react'
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Title from '../atom/Title'
import Button from '../atom/Button'

class Meetup extends Component{
    render(){
        const {title, countMeet, titleMeet, countPeople} = this.props
        return(
            <Fragment>
                <Card style={{backgroundColor:'#ffbcc4', width:'30%', display:'inline-block', margin:5}}>
                    <CardContent>
                        <Title variant="h6"><b>{title}</b></Title>
                        <hr/>
                        <p>
                            <b><span>{countMeet}</span> {titleMeet}</b> 
                        </p>
                        <p><b>{countPeople}</b> <span style={{color:'#fb929e'}}>went</span></p>
                        <Button variant="contained" color="primary">View</Button>
                    </CardContent>
                </Card>
            </Fragment>
        )
    }
}
export default Meetup