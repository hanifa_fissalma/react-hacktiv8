import React, {Component, Fragment} from 'react'
import AvatarPict from '../atom/Avatar'

const style={
    box:{
        padding: 15,
        backgroundColor:'#aedefc',
        margin: 10
    },
    div1:{
        width:'15%', 
        padding:5, 
        display:'inline-block',
        verticalAlign: 'middle'
    },
    div2:{
        width:'75%', 
        padding:5, 
        display:'inline-block',
        verticalAlign: 'middle'
    }
}
class Members extends Component{
    render(){
        const {name,height} = this.props
        return(
            <Fragment>
                <div style={style.box}>
                    <div style={style.div1}>
                        <AvatarPict />
                    </div>
                    <div style={style.div2}>
                        <b>{name}</b>
                        <br/>
                        <p>{height} cm </p>
                    </div>
                </div>
            </Fragment>
        )
    }
}
export default Members