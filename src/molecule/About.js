import React, {Fragment, Component} from 'react'
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Title from '../atom/Title'

class About extends Component{
    render(){
        const {message, socialMedia, account, hashtag} = this.props
        return(
            <Fragment>
                <Title variant="h5" color="inherit"><b>About Meetup</b></Title>
                <Card>
                    <CardContent>
                        <p>{message}</p>
                        <p>
                            {socialMedia}: {account} and we use the hashtag {hashtag}
                        </p>
                    </CardContent>
                </Card>
            </Fragment>
        )
    }
}

export default About