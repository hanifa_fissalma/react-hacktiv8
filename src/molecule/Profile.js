import React, {Component} from 'react'
import Button from '../atom/Button'
import Title from '../atom/Title'
import ProfilePict from '../atom/ProfilePict'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import {connect} from 'react-redux'
import {changePageTitle, changeCopyrightYear} from '../action/app'
const style={
    row:{
        fontSize:'1.05rem',
        borderBottom:'none'
    },
    box:{
        padding: 15,
        backgroundColor:'#aedefc'
    },
    div:{
        width:'40%', 
        padding:5, 
        display:'inline-block',
        verticalAlign: 'middle'
    },
    button:{
        width:150
    }
}
class Profile extends Component{
    render(){
        const {image, location, member, organizer} = this.props
        return(
            <Paper style={style.box}>
                <div style={style.div}>
                    <ProfilePict 
                        src={image}
                    />
                </div>
                <div style={style.div}>
                    <Title variant="h4" color="inherit"><b>Hactiv8 Meetup</b></Title>
                    <br/>
                    <Table>
                        <TableBody>
                            <TableRow>
                                <TableCell style={style.row}>Location</TableCell>
                                <TableCell style={style.row}>{location}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={style.row}>Member</TableCell>
                                <TableCell style={style.row}>{member}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell style={style.row}>Organizer</TableCell>
                                <TableCell style={style.row}>{organizer}</TableCell>
                            </TableRow>
                        </TableBody>
                    </Table>
                    <br/>
                    <Button variant="contained" color="primary" style={style.button}>Join Us</Button>
                    <br/><br/>
                    <Button variant="contained" color="secondary" style={style.button} onClick={() => this.props.magicButton('qtmuan kuy')}>
                        Magic Button
                    </Button>
                    <br/><br/>
                    <Button variant="contained" color="secondary" style={style.button} onClick={() => this.props.changeYear(1994)}>Change Year</Button>
                </div>
            </Paper>
        )
    }
}
const mapDispatchToProps = (dispatch) =>{
    return{
        magicButton: (judulBaru) => dispatch(changePageTitle(judulBaru)),
        changeYear: (tahunBaru) => dispatch(changeCopyrightYear(tahunBaru))
    }
}
export default connect(null,mapDispatchToProps)(Profile)