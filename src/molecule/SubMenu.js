import React, {Fragment, Component} from 'react'
import Title from '../atom/Title'
const style={
    div:{
        width:'45%', 
        padding:5, 
        display:'inline-block',
        verticalAlign: 'middle'
    }
}
class SubMenu extends Component{
    render(){
        const {title} = this.props
        return(
            <Fragment>
                <div style={style.div}>
                    <Title variant="h5"><b>{title}</b></Title>
                </div>
                <div style={style.div}>
                    <a href="http://localhost:3000" style={{float:'right', margin:5}}>see all</a>
                </div>
            </Fragment>
        )
    }
}
export default SubMenu