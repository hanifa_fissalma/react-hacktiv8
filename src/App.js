import React from 'react'
import './App.css'
import Header from './molecule/Header'
import Footer from './atom/Footer'
import { BrowserRouter, Route} from "react-router-dom";
import Home from './pages/Home'
import AboutEvent from './pages/AboutEvent'
import Show from './pages/Show'
import {Provider} from 'react-redux'
import store from './config/store'

class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <BrowserRouter>
          <div>
            <Header />
            <Route exact path="/" component={Home} />
            <Route path="/about" component={AboutEvent} />
            <Route path="/show/:id" component={Show} />
            <Footer/>
          </div>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default App;
