import React from 'react'
import Divider from '@material-ui/core/Divider';
import {connect} from 'react-redux'
const Footer = (props) => {
    return(
        <div>
            <Divider />
            <br/>
            <center><b>{props.app.pageTitle}-{props.app.copyRightYear}</b></center>
        </div>
    )
}
const mapStateToProps = (state) =>{
    return{
        app: state.app
    }
}
export default connect(mapStateToProps)(Footer)