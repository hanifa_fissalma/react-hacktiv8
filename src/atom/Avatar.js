import React from 'react'
import Avatar from '@material-ui/core/Avatar';
const style={
    width:100,
    height:100,
    backgroundColor: 'black'
}
const AvatarPict = (props) =>{
    return(
        <Avatar style={style} {...props} />
    )
}
export default AvatarPict