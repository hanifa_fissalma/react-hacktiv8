import React from 'react'
const style={
    foto:{
        width:300,
        height:300
    }
}
const ProfilePict = (props) =>{
    return(
        <img style={style.foto} alt="foto" {...props}/>
    )
}
export default ProfilePict