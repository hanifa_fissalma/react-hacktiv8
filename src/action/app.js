export const changePageTitle = (newPageTitle) => {
    return{
        type:'PAGE_TITLE_CHANGE',
        payload:{
            newpageTitle: newPageTitle
        }
    }
}

export const changeCopyrightYear = (newCopyrightYear) => {
    return{
        type:'COPYRIGHT_YEAR_CHANGE',
        payload:{
            newCopyrightYear: newCopyrightYear
        }
    }
}