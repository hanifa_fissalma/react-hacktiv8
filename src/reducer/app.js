const InitialState = {
    pageTitle:'QTemuanYuk',
    copyRightYear: 2018
}
const appReducer = (state = InitialState,action) =>{
    switch(action.type){
        case 'PAGE_TITLE_CHANGE':
            return {
                ...state,
                pageTitle:action.payload.newPageTitle
            }
        case 'COPYRIGHT_YEAR_CHANGE':
            return{
                ...state,
                copyRightYear:action.payload.newCopyrightYear
            }
        default:
            return state
    }
    
}
export default appReducer