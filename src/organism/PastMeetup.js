import React, {Component} from 'react'
import Meetup from '../molecule/Meetup'

class PastMeetup extends Component{
    render(){
        return(
            <Meetup style={{display:'inline'}} {...this.props}/>  
        )
    }
}
export default PastMeetup