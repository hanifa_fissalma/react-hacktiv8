import {createStore, combineReducers, applyMiddleware} from 'redux'
import memberReducer from '../reducer/member'
import appReducer from '../reducer/app'
import logger from 'redux-logger'
import thunk from 'redux-thunk'
const reducers = combineReducers({
    member:memberReducer,
    app:appReducer
})
const middleware = applyMiddleware(logger, thunk)
export default createStore(reducers, middleware)